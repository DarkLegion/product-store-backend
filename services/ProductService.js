const Service = require('../libs/Service.js');

module.exports = class ProductService extends Service {
    async update(id, product) {
        delete product.name;
        let newStock = product.stocks[0];
        let stockModel = this.app.models.inventory.Stock;
        let stock = await stockModel.findById(product.stocks[0]._id);
        Object.assign(stock, newStock);
        await stock.save();
        product.stocks = [stock._id];
        return this.app.models.inventory.Product.findByIdAndUpdate(id, product);
    }

    async create(product) {
        let stockModel = this.app.models.inventory.Stock;
        let stock = new stockModel(product.stocks[0]);
        await stock.save();
        product.stocks = [stock._id];
        return this.app.models.inventory.Product.create(product);
    }

    find(filter = {}, skip = 0, limit = 10) {
        return this.app.models.inventory.Product.find(filter).skip(Number(skip)).limit(Number(limit)).populate('stocks');
    }

    findById(id) {
        return this.app.models.inventory.Product.findById(id).populate('stocks');
    }

    async delete(id) {
        let productDB = await this.app.models.inventory.Product.findById(id);
        let ordersProducts = await this.app.models.payment.OrderProduct.findOne({ product: id });
        if (ordersProducts) {
            throw new Error('The product is assigned to a one or more orders');
        }
        await this.app.models.inventory.Stock.findByIdAndRemove(productDB.stocks[0]._id);
        return this.app.models.inventory.Product.findByIdAndRemove(productDB._id);
    }
}