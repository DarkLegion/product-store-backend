const AppServices = require('../libs/AppServices');
const models = require('../models');

module.exports = (app) => {
    return new AppServices(app, {
        ProductService: require('./ProductService'),
        StockService: require('./StockService'),
        OrderService: require('./OrderService'),
        OrderProductService: require('./OrderProductService'),
        ConfigurationService: require('./ConfigurationService'),
        CompanyService: require('./CompanyService'),
        AuthService: require('./AuthService') 
    })
};