const Service = require('../libs/Service.js');

module.exports = class ConfigurationService extends Service {
    findOne(filter) {
        return this.app.models.business.Configuration.findOne(filter);
    }
    find(filter = {}, skip = 0, limit = 10) {
        return this.app.models.business.Configuration.find(filter).skip(Number(skip)).limit(Number(limit));
    }    
}