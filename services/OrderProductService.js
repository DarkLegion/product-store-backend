const Service = require('../libs/Service.js');
const BBPromise = require('bluebird');

module.exports = class OrderProductService extends Service {
    destroy(id){
        return this.app.models.payment.OrderProduct.findByIdAndRemove(id);
    }    
}