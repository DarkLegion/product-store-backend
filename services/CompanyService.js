const Service = require('../libs/Service.js');

module.exports = class CompanyService extends Service {
    findOne() {
        return this.app.models.business.Company.findOne();
    }
}