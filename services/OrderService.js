const Service = require('../libs/Service.js');
const BBPromise = require('bluebird');

module.exports = class OrderService extends Service {

    find(filter = {}, skip = 0, limit = 10) {
        return this.app.models.payment.Order.find(filter).skip(Number(skip)).limit(Number(limit)).populate('creditCard');
    }

    async create(order) {
        let OrderProductModel = this.app.models.payment.OrderProduct;
        let orderProductIds = [];
        await BBPromise.map(order.orderProducts, (orderProduct) => {
            let orderProductDB = new OrderProductModel(orderProduct);
            orderProductIds.push(orderProductDB._id);
            return orderProductDB.save();
        });
        order.orderProducts = orderProductIds

        return this.app.models.payment.Order.create(order);
    }
    findById(id) {
        return this.app.models.payment.Order.findById(id).populate([{ path: 'orderProducts' }, { path: 'creditCard' }]);
    }

    async destroy(id) {
        let order = await this.findById(id);
        let orderProducts = order.orderProducts || [];
        await BBPromise.map(orderProducts, (orderProduct) => {
            return orderProduct.remove()
        });
        let creditCard = order.CreditCard; 
        await creditCard && CreditCard.remove();
        return order.remove();

    }

    async update(id, fieldsToUpdate) {
        let creditCardDB = new this.app.models.payment.CreditCard(fieldsToUpdate.creditCard);
        fieldsToUpdate.creditCard = creditCardDB._id;
        await creditCardDB.save();
        return this.app.models.payment.Order.findByIdAndUpdate(id, { $set: fieldsToUpdate }, { new: true })
    }

}