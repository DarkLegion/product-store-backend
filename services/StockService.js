const Service = require('../libs/Service.js');

module.exports = class StockService extends Service {
  findOne(filter = {}) {
    return this.app.models.inventory.Stock.findOne(filter).sort({ date: -1 });
  }
}