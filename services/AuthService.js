const Service = require('../libs/Service.js');
const BBPromise = require('bluebird');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const tokenSecret = require('../token.json').secret;

module.exports = class UserService extends Service {

    async login(userInfo) {
        let user = await this.app.models.business.User.findOne({ name: userInfo.name });
        if (user) {
            let isValidPassword = await this.comparePassword(userInfo.password, user.password);
            if (isValidPassword) {
                let token = await this.genToken(userInfo, tokenSecret);
                return { user, token }
            } else {
                throw new Error('Invalid Password');
            }
        } else {
            throw new Error('Invalid User');
        }
    }

    genToken(data) {
        return new BBPromise((resolve, reject) => {
            jwt.sign({ data }, tokenSecret, { algorithm: 'HS512', expiresIn: '1h' }, (err, token) => {
                if (err) return reject(err)
                resolve(token);
            })
        })
    }

    comparePassword(password, dbHash) {
        return new BBPromise((resolve, reject) => {
            bcrypt.compare(password, dbHash, function (err, res) {
                if (err) return reject(e);
                resolve(res);
            });
        })
    }

    validateToken(token) {
        return new BBPromise((resolve, reject) => {
            jwt.verify(token, tokenSecret, { algorithms: ['HS512'] }, (err, decoded) => {
                if (err) return reject(err)
                resolve(decoded);
            })
        })
    }
}