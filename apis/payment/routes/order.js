const express = require('express')
const router = express.Router();

router.post('/', async (req, res, next) => {
  let order = req.body;
  let orderDB = await req.app.locals.services.OrderService.create(order);
  res.json(orderDB);
});

router.put('/:id', async (req, res, next) => {
  let id = req.params.id;
  let orderUpdateFields = req.body;
  let orderDB = await req.app.locals.services.OrderService.update(id, orderUpdateFields);
  res.json(orderDB);
});

router.get('/', async (req, res, next) => {
  let params = req.query;
  let filter = params.filter ? JSON.parse(params.filter) : {};
  let ordersDB = await req.app.locals.services.OrderService.find(filter, params.skip, params.limit);
  res.json(ordersDB);
});

router.get('/:id', async (req, res, next) => {
  let id = req.params.id;
  let orderDB = await req.app.locals.services.OrderService.findById(id);
  res.json(orderDB);
});

router.delete('/:id', async (req, res, next) => {
  try {
    let id = req.params.id;
    let orderDB = await req.app.locals.services.OrderService.destroy(id);
    res.json(orderDB);
  } catch(e){
    next(e);
  }
});


module.exports = router;