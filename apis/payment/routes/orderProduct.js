const express = require('express')
const router = express.Router();

router.delete('/:id', async (req, res, next) => {
  let id = req.params.id;
  let orderProductDB = await req.app.locals.services.OrderProductService.destroy(id);
  res.json(orderProductDB);
});

module.exports = router;