const express = require('express')
const router = express.Router();

router.use('/orders', require('./order'));
router.use('/orderproducts', require('./orderProduct'));

module.exports = router;