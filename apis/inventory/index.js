const express = require('express');
const app = require('express')();
const config = require('./config');
const path = require('path');

app.locals = require('../../app');

app.use('/inventory', require('./routes'));

app.use('/inventory/assets', express.static(path.join(__dirname, './assets')));

app.use(function (err, req, res, next) {
  if (err) {
    res.status(500).json({ message: err.message, stack: err.stack });
  } else {
    next();
  }
});

//registering the api

app.locals.libs.utilities.registerApi({
  params: {
    path: 'inventory',
    url: config.url
  },
  gateway: config.gateway
}).then(() => {
  app.listen(config.port);
}).catch((e) => {
  console.error(e);
});

