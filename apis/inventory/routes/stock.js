const express = require('express')
const router = express.Router();

router.get('/', async (req, res, next) => {
  let params = req.query;
  let filter = params.filter ? JSON.parse(params.filter) : {};
  if (params.findOne === 'true') {
    let products = await req.app.locals.services.StockService.findOne(filter);
    res.json(products);
  } else {
    next();
  }
});

module.exports = router;