const express = require('express')
const router = express.Router();
const uploadImgHandler = require('../../../middleware/product-image-upload');
const verifyToken = require('../../../middleware/verify-token');

router.post('/', verifyToken, uploadImgHandler, async (req, res, next) => {
  let product = req.body;
  product.stocks = JSON.parse(product.stocks);
  product.img = req.file.filename
  try {
    let productDB = await req.app.locals.services.ProductService.create(product);
    res.json(productDB);
  } catch (e) {
    next(e);
  }
});

router.put('/:id', verifyToken, uploadImgHandler, async (req, res, next) => {
  let product = req.body;
  let id = req.params.id;
  product.stocks = JSON.parse(product.stocks);
  product.img = req.file.filename
  try {
    let productDB = await req.app.locals.services.ProductService.update(id, product);
    res.json(productDB);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', uploadImgHandler, async (req, res, next) => {
  let id = req.params.id;
  try {
    let productDB = await req.app.locals.services.ProductService.delete(id);
    res.json(productDB);
  } catch (e) {
    next(e);
  }
});

router.get('/', async (req, res, next) => {
  let params = req.query;
  let filter = params.filter ? JSON.parse(params.filter) : {};
  let products = await req.app.locals.services.ProductService.find(filter, params.skip, params.limit);
  res.json(products);
});

router.get('/:id', async (req, res, next) => {
  let id = req.params.id;
  let product = await req.app.locals.services.ProductService.findById(id);
  res.json(product);
});

module.exports = router;