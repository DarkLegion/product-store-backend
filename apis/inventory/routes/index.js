const express = require('express')
const router = express.Router();

router.use('/products', require('./product'));
router.use('/stocks', require('./stock'));

module.exports = router;