const express = require('express')
const router = express.Router();

router.get('/:name', async (req, res, next) => {
  let name = req.params.name;
  let config = await req.app.locals.services.ConfigurationService.findOne({ name });
  res.json(config);
});

router.get('/', async (req, res, next) => {
  let params = req.query;
  let filter = params.filter ? JSON.parse(params.filter) : {};
  let configurations = await req.app.locals.services.ConfigurationService.find(filter, params.skip, params.limit);
  res.json(configurations);
});

module.exports = router;