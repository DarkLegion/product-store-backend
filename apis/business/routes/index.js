const express = require('express')
const router = express.Router();

router.use('/configurations', require('./configuration'));
router.use('/companies', require('./company'));
router.use('/auth', require('./auth'));

module.exports = router;