const express = require('express')
const router = express.Router();

router.get('/', async (req, res, next) => {  
  let product = await req.app.locals.services.CompanyService.findOne();
  res.json(product);
});

module.exports = router;