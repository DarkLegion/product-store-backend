const express = require('express')
const router = express.Router();

router.post('/', async (req, res, next) => {
  let userInfo = req.body;
  try {
    let userData = await req.app.locals.services.AuthService.login(userInfo);
    res.json(userData);
  } catch (e) {
    next(e);
  }
});

router.post('/token', async (req, res, next) => {
  let token = req.body.token;
  try {
    let userData = await req.app.locals.services.AuthService.validateToken(token);
    res.json(userData);
  } catch (e) {
    next(e);
  }
});


module.exports = router;