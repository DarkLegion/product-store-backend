const express = require('express');
const app = require('express')();
const config = require('./config');
const bodyParser = require('body-parser');

app.locals = require('../../app');
app.locals.config = config;
app.use(bodyParser.json());
app.use('/business', require('./routes'));

app.use(function (err, req, res, next) {
  if (err) {
    res.status(500).json({ message: err.message, stack: err.stack });
  } else {
    next();
  }
});

//registering the api
app.locals.libs.utilities.registerApi({
  params: {
    path: 'business',
    url: config.url
  },
  gateway: config.gateway
}).then(() => {
  app.listen(config.port);
}).catch((e) => {
  console.error(e);
});

