const uuidV4 = require('uuid/v4');
module.exports = {
  port: 3003,
  tokenSecret: uuidV4(),
  url: 'http://localhost:3003',
  gateway: 'http://localhost:3000'
}