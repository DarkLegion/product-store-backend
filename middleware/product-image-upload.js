const multer = require('multer')
const path = require('path');
const uuidV4 = require('uuid/v4');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './apis/inventory/assets/img')
  },
  filename: function (req, file, cb) {    
    cb(null, uuidV4() + path.extname(file.originalname))
  }
})

module.exports = multer({storage}).single('img');
