const request = require('request');

module.exports = async (req, res, next) => {
  let authorization = req.get('Authorization');
  let token = authorization.replace(/^Bearer /,'');
  try{
    let user = await req.app.locals.services.AuthService.validateToken(token);
    next();
  }catch(e){
    next(e);
  }
}
