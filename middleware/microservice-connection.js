const request = require('request');

module.exports = (req, res, next) => {
  let splitUrl = req.originalUrl.split('/');
  let apiPath = splitUrl[1];
  let apiConfig = req.app.locals.apis[apiPath];
  if (apiConfig) {
    req.pipe(request({
      uri: apiConfig.url + req.originalUrl,
      method: req.method
    })).pipe(res);
  } else {
    return next();
  }
}



