const app = require('../app');
const fs = require('fs');
const BBPromise = require('bluebird');

const ProductModel = app.models.inventory.Product;
const StockModel = app.models.inventory.Stock;
const ConfigurationModel = app.models.business.Configuration;
const CompanyModel = app.models.business.Company;
const UserModel = app.models.business.User;

(async () => {

  //products  
  await BBPromise.map(require('./data/products'), async (product) => {
    try {
      let stock = product.stocks[0];
      let stockDB = new StockModel(stock);
      let productDB = new ProductModel(product);
      productDB.set('stocks', [stockDB._id]);
      stockDB.set('productId', productDB._id);
      await productDB.save();
      await stockDB.save();
    } catch (e) {
      console.error(e);
    }
  })
  console.info('Product seeding complete');

  //configuration  
  await BBPromise.map(require('./data/configuration'), async (configuration) => {
    try {
      await ConfigurationModel.create(configuration);
    } catch (e) {
      console.error(e);
    }
  })
  console.info('Configuration seeding complete');

  //Company    
  try {
    await CompanyModel.create(require('./data/company'));
  } catch (e) {
    console.error(e);
  }
  console.info('Company seeding complete');

  //User    
  try {
    let user = new UserModel(require('./data/user'));
    await user.save();
  } catch (e) {
    console.error(e);
  }
  console.info('User seeding complete');

  console.info('Seeding finished');
})();