let app = {};
app.models = require('./models')(app);
app.services = require('./services')(app);
app.libs = {
  utilities: new (require('./libs/utilities'))()
}
module.exports = app;