const express = require('express')
const router = express.Router();

//express body parser has issues when use the app as proxy with post methods
//registers the api
router.get('/register', (req, res, next) => {
  let apiInfo = JSON.parse(req.query.apiInfo);
  req.app.locals.apis[apiInfo.path] = {
    url: apiInfo.url
  }
  res.json({ response: 'ok' });
});

module.exports = router;
