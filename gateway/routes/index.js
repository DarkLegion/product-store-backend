const express = require('express')
const router = express.Router();

router.use('/microservices', require('./microservices'));

module.exports = router;
