const config = require('./config');
const app = require('express')();
const microserviceConnection = require('../middleware/microservice-connection');
const cors = require('cors')
app.locals.apis = {};

//Routes

app.use(cors());

app.use(require('./routes'));

app.use(microserviceConnection);

app.listen(config.port);