const Schema = require('mongoose').Schema;

module.exports = class MongooseModel {

    constructor(app, mongooseInstance) {
        this.mongoose = mongooseInstance;
        this.app = app;
    }

    get modelName() {
        throw new Error('No model name specified');
    }

    attributes(Schema) {
        throw new Error('No attributes specified');
    }

    execute(Schema) {

    }

    plugins(modelSchema){

    }

    generateModel() {
        let modelSchema = new Schema(this.attributes(Schema));
        this.plugins(modelSchema);
        this.execute(modelSchema);
        let Model = this.mongoose.model(this.modelName, modelSchema);        
        return Model;
    }
}