const mongoose = require('mongoose');
const config = require('../models/config');
const BBPromise = require('bluebird');

module.exports = class AppMongoose {

    constructor(app, database, models) {
        this._config = config;
        this._instance = this.constructor._createInstance(app, database);
        this.constructor._compileModels(app, models, this._instance, this);
    }

    static _createInstance(app, database) {
        let conf = config.databases[database];
        let authentication = conf.username && conf.password ? `${conf.username}:${conf.password}@` : '';
        mongoose.Promise = BBPromise;
        let db = mongoose.createConnection(`mongodb://${authentication}${conf.host}:${conf.port}/${database}`, config.options);
        return db;
    }

    static _compileModels(app, models, instance, that) {
        for (let key in models) {
            that[key] = (new models[key](app, instance)).generateModel();
        }
    }
}