const request = require('request-promise');

module.exports = class Utilites {
  registerApi({ params, gateway }) {
    return request({
      method: 'GET',
      uri: `${gateway}/microservices/register?apiInfo=${JSON.stringify(params)}`
    });
  }
}