module.exports = class AppServices {
    constructor(app, services) {
        this.constructor._compileServices(app, services, this);
    }

    static _compileServices(app, services, that) {
        for (let key in services) {
            that[key] = new services[key](app);
        }
    }
}