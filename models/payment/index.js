const AppMongoose = require('../../libs/AppMongoose');

module.exports = (app) => {
    return new AppMongoose(app, 'payment', {
        OrderProduct: require('./OrderProduct'),
        CreditCard: require('./CreditCard'),
        Order: require('./Order')
    });
} 