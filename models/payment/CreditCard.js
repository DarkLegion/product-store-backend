const MongooseModel = require('../../libs/MongooseModel');

module.exports = class CreditCardModel extends MongooseModel {

    get modelName() {
        return 'CreditCard'
    }

    attributes(Schema) {
        return {
            name:String,
            number:String,
            expiration:String,
            securityCode:String            
        }
    }
}