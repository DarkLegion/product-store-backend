const MongooseModel = require('../../libs/MongooseModel');

module.exports = class OrderModel extends MongooseModel {

    get modelName() {
        return 'OrderProduct'
    }

    attributes(Schema) {
        return {
            date: { type: Date, default: Date.now() },
            product: Schema.Types.ObjectId,
            quantity: Number
        }
    }
}