const MongooseModel = require('../../libs/MongooseModel');

module.exports = class OrderModel extends MongooseModel {

    get modelName() {
        return 'Order'
    }

    attributes(Schema) {
        return {
            tax: Number,
            date: { type: Date, default: Date.now() },
            status: String,
            orderProducts: [{ type: Schema.Types.ObjectId, ref: 'OrderProduct' }],
            creditCard: { type: Schema.Types.ObjectId, ref: 'CreditCard' }
        }
    }
}