module.exports = {
    options: {
        server: { poolSize: 5 }
    },
    databases: {
        inventory: {
            host: 'localhost',
            port: 27017
        },
        auth: {
            host: 'localhost',
            port: 27017
        },
        payment: {
            host: 'localhost',
            port: 27017
        },
        business: {
            host: 'localhost',
            port: 27017
        }
    }
}