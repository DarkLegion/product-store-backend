module.exports = (app) => {
    return {
        inventory: require('./inventory')(app),
        payment: require('./payment')(app),
        business: require('./business')(app)
    }
}