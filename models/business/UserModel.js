const MongooseModel = require('../../libs/MongooseModel');
const bcrypt = require('bcrypt');

module.exports = class UserModel extends MongooseModel {

    get modelName() {
        return 'User'
    }

    attributes(Schema) {
        return {
            name: { type: String, unique: true },
            password: String,
            Company: [{ type: Schema.Types.ObjectId, ref: 'Company' }]
        }
    }

    execute(UserSchema) {

        UserSchema.pre('save', function (next, data){            
            bcrypt.hash(this.password, 12, (err, hash) => {                
                if (err) throw err;
                this.password = hash;
                next();
            });
        });

        UserSchema.pre('update', function (next){
            bcrypt.hash(this.password, 12, (err, hash) => {
                if (err) throw err;
                this.password = hash;
                next();
            });
        });
    }
}