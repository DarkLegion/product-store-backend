const MongooseModel = require('../../libs/MongooseModel');

module.exports = class ConfigurationModel extends MongooseModel {

    get modelName() {
        return 'Configuration'
    }

    attributes(Schema) {
        return {
            name: { type: String, unique: true },
            value: String,
            Company: [{ type: Schema.Types.ObjectId, ref: 'Company' }]
        }
    }
}