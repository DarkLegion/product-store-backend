const AppMongoose = require('../../libs/AppMongoose');

module.exports = (app) => {
    return new AppMongoose(app, 'business', {
        Company: require('./CompanyModel'),
        Configuration: require('./ConfigurationModel'),
        User: require('./UserModel')
    });
} 