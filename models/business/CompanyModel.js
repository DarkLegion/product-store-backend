const MongooseModel = require('../../libs/MongooseModel');

module.exports = class ConfigurationModel extends MongooseModel {

    get modelName() {
        return 'Company'
    }

    attributes(Schema) {
        return {
            name: { type: String, unique: true },
            direction: String,
            contactNumber: String,
            configuration: [{ type: Schema.Types.ObjectId, ref: 'Configuration' }],
            users: [{ type: Schema.Types.ObjectId, ref: 'User' }]
        }
    }
}