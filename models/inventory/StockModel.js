const MongooseModel = require('../../libs/MongooseModel');

module.exports = class StockModel extends MongooseModel {

  get modelName() {
    return 'Stock'
  }

  attributes(Schema) {
    return {
      quantity: Number,
      date: { type: Date, default: Date.now() },
      price: Number,
      productId: { type: Schema.Types.ObjectId, ref: 'Product' }
    }
  }
}