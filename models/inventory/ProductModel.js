const MongooseModel = require('../../libs/MongooseModel');

module.exports = class ProductModel extends MongooseModel {

    get modelName() {
        return 'Product'
    }

    attributes(Schema) {
        return {
            name: { type: String, unique: true },
            date: { type: Date, default: Date.now() },
            img: String,
            stocks: [{ type: Schema.Types.ObjectId, ref: 'Stock' }]
        }
    }
}