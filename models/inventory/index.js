const AppMongoose = require('../../libs/AppMongoose');

module.exports = (app) => {
    return new AppMongoose(app, 'inventory', {
        Product: require('./ProductModel'),
        Stock: require('./StockModel')
    });
} 